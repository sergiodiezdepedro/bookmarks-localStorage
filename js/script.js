const formulario = document.getElementById('formulario');

// Listener para el envío del formulario
formulario.addEventListener('submit', guardarFavorito);

function guardarFavorito(e) {

    // Captura del nombre del sitio
    let nombreSitio = document.getElementById('nombreSitio').value;
    // Captura de la dirección del sitio
    let urlSitio = document.getElementById('urlSitio').value;

    if (!validacionDatos(nombreSitio, urlSitio)) {
        return false;
    }

    // Objeto para guardar las variables
    let favorito = {
        nombre: nombreSitio,
        direccion: urlSitio
    };

    // Detección de que bookmarks esté vacío
    if (localStorage.getItem('bookmarks') === null) {
        //Iniciar el array
        let bookmarks = [];
        // Añadir el ítem al array
        bookmarks.push(favorito);
        // Enviarlo a LocalStorage
        localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    } else {
        // Obtener los favoritos de LocalStorage
        let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
        //Añadir el favorito al array
        bookmarks.push(favorito);
        // Resetear LocalStorage
        localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
    }

    // Borrar los inputs tras registrar un sttio
    formulario.reset();

    // Reconstituir en pantalla los favoritos tras añadir uno
    mostrarFavoritos();

    e.preventDefault();
}

// Borrar favorito
function borrarFavorito(direccion) {
    // Capturar los favoritos existentes en LocalStorage
    let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
    // Buscar entre los favoritos
    for (var i = 0; i < bookmarks.length; i++) {
        if (bookmarks[i].direccion == direccion) {
            // Borrar el favorito del array
            bookmarks.splice(i, 1);
        }
    }
    // Resetear LocalStorage
    localStorage.setItem('bookmarks', JSON.stringify(bookmarks));

    // Reconstituir en pantalla los favoritos tras el borrado 
    mostrarFavoritos();
}

// Mostar los favoritos
function mostrarFavoritos() {
    let bookmarks = JSON.parse(localStorage.getItem('bookmarks'));

    // Capturar el id del elemento del DOM donde se van a mostrar los favoritos
    let bookmarksPresentes = document.getElementById('bookmarksPresentes');

    // Construir la salida de los datos
    bookmarksPresentes.innerHTML = '';

    for (var i = 0; i < bookmarks.length; i++) {
        let nombre = bookmarks[i].nombre;
        let direccion = bookmarks[i].direccion;

        bookmarksPresentes.innerHTML += `<div class="well">
                                            <h3>
                                                ${nombre}
                                                <a class="btn btn-success" target="_blank" href="${direccion}">Visitar</a>
                                                <a onclick="borrarFavorito('${direccion}')" class="btn btn-danger" href="#">Borrar</a>
                                            </h3>
                                        </div>`;

    }
}

// Validación de la entrada de datos
function validacionDatos(nombreSitio, urlSitio) {
    if (!nombreSitio || !urlSitio) {
        alert('Rellena el nombre del sitio y la dirección');
        return false;
    }

    let expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    let regex = new RegExp(expression);

    if (!urlSitio.match(regex)) {
        alert('Hay que introducir una URL válida');
        return false;
    }

    return true;
}

window.addEventListener("load", mostrarFavoritos);