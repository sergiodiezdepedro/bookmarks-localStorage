# Favoritos en localStorage

Este es un sencillo ejercicio para repasar un puñado de puntos básicos en programación JavaScript.

Obviamente, es muy mejorable. Los favoritos se almacenan mediante **localStorage**. Un paso significativo sería crear una API para almacenarlos en la nube.

Como el objetivo último era seguir avanzando en el conocimiento del lenguaje citado, maquetar no ha sido una prioridad, por lo que he recurrido a una [plantilla de Bootstrap](http://getbootstrap.com/examples/jumbotron-narrow/).

Para que el  script funcione, las urls deben están construidas con `http://` o `https://`.